#  Array methods
### 1. join() = It joins the elements of array    as a String.
#### Example:
arr= [r,a,j,a,t] 

arr.join('')

return =["rajat"]

 ### 2. Flat() = It creates a new array carrying sub-array elements concatenated to the specified depth.
#### Example:
arr= [[r,a],j,[[a,t]]] 

arr.flat()

return=[r,a,j,a,t]

 ### 3. push()= It adds a new value to the end of an array.
 #### Example:
 arr=[r,a,j,a] 

 arr.push(t)

 arr=[r,a,j,a,t]

 ### 4. indexOf()=It searches the specified element in the given array and returns the index of the first match.

 #### Example:
  arr=[r,a,j,a,t]

  arr.indexOf(a) 

  ans=1
### 5. lastIndexOf()=It searches the specified element in the given array and returns the index of the last match.
#### Example:
arr=[r,a,j,a,t]

arr.lastIndexOf(a)

return = 3.
### 6. includes()= It checks whether the given array contains the specified element.
#### Example:
arr=[r,a,j,a,t]

arr.includes(t)

ans=true.
### 7. reverse()=It reverse the elements of given array.
#### Example:
arr=[r,a,j,a,t]

arr.reverse()

arr=[t,a,j,a,r]

### 8. every()=It determines whether all elements of an array are satisfying the provided function conditions.
#### Example:
arr=[1,2,3,4,5]

condition:

(arr[i]<6)

arr.every(condition)

return =true


### 9. shift()=It removes and returns the first element of an array.
#### Example:
 arr=[r,a,j,a,t]

 arr.shift()

 return = r
 ### 10. splice()=It add/remove elements from the array.
 ### Example:
 arr=[r,a,j,a,t]

arr.splice(1,1,i)

return =[r,i,j,a,t]
### 11.find()=It returns the value of the first element that satisfies the condition.
#### Example:
arr=[r,a,j,a,t]

arr.find(a)

return = a
### 12. unshift()=It adds one or more elements at the beginning of the array.
#### Example:
arr=[r,a,j,a,t]

arr.unshift(a)

arr=[a,r,a,j,a,t]
### 13. findIndex()=It returns the index value of the first match that satisfies the given condition.
#### Example:
arr=[r,a,j,a,t]

arr.findIndex(a)

return=3.

### 14. filter()=It returns the new array containing the elements that pass the provided condition.
#### Example:
arr=[r,a,j,a,t]

arr.filter(e=> e=a)

return = [a,a].
### 15. forEach()=It iterates once for each element of an array.
#### Example:
arr=[r,a,j,a,t]

arr.forEach()

return =[r,a,j,a,t]

### 16. map()=It calls the specified function for every element and returns a new array.
#### Example:
arr=[4,9,16,25]

arr.map(Math.sqrt)

return [2,3,4,5].

### 17. pop()=It removes and returns the last element of an array.
#### example:
 arr=[r,a,j,a,t]

 arr.pop()

 return= t.
 ### 18. reduce()=It executes a provided function for each value and reduces the array to a single value.
 #### Example:
 arr=[25,40,45,50]

 arr.reduce((acc,curr)=>{

    acc+=curr

    return acc
    
   },0);

 return =160.

 ### 19. slice()=It returns a new array containing the copy of part of the given array.
 #### Example:
 arr=[r,a,j,a,t]

 arr.slice(1,3)

 return=[r,t]
 ### 20. some()=It determines if any element of the array passes the test of the implemented function.
 #### Example:
 arr=[1,2,3,4]

 condition:

 if(arr(element)>3)

 return=true.


